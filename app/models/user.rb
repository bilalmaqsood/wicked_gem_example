class User < ActiveRecord::Base

  validates :password, confirmation: true

	validates :twitter_username, presence: true, if: -> { required_for_step?(:social) }

	validates :date_of_birth, presence: true, if: -> { required_for_step?(:personal)}

  cattr_accessor :form_steps do
		%w(personal social)
	end

	attr_accessor :form_step
	attr_writer :current_step

  def required_for_step?(step)
		return true if form_step.nil?
		return true if self.form_steps.index(step.to_s) <= self.form_steps.index(form_step)
	end


end
