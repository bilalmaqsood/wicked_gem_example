class UserStepsController < ApplicationController
  include Wicked::Wizard

	steps  *User.form_steps

	def show
		@user = User.find_by_id(session[:user_id])
		render_wizard
	end

	def update
		@user = User.find_by_id(session[:user_id])
		@user.attributes = user_params
		render_wizard @user
		
	end

	private
	def user_params
		permitted_attributes = case step
													 when "personal"
														 [:date_of_birth, :bio, :name]
													 when "social"
														 [:website, :twitter_username, :github_username]
													 end
		params.require(:user).permit(permitted_attributes).merge(form_step: step)
	end

end
