class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password
      t.string :name
      t.text :bio
      t.string :website
      t.string :twitter_username
      t.string :github_username
      t.date :date_of_birth

      t.timestamps null: false
    end
  end
end
